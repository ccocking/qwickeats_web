<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/**
 * Route to handle navigation to the root 
 *
 * @return View - Landing Page
 * @author Christian Cocking <cc@pith-studio.com>
 */
Route::get('/', array("before" => "guest",function()
{
	return View::make('landing');
}));

Route::get('users/{id}/account', array(
	'before' => 'auth',
	'as' => 'get_settings',
	'uses' => 'UsersController@get_settings'
));

Route::post('restaurants/{id}/checkout', array(
	'as' => 'get_checkout',
	'uses' => 'RestaurantsController@CheckoutPage'
	));

Route::get('restaurants/apply_filter', array(
	'as' => 'apply_filter',
	'uses' => 'RestaurantsController@apply_filter'
	));

Route::get('restaurants/remove_filter', array(
	'as' => 'remove_filter',
	'uses' => 'RestaurantsController@remove_filter'
	));

Route::post('update_account_settings', array(
	'as' => 'update_account_settings',
	'uses' => 'UsersController@update_account'
	));

Route::get('logout', function(){
	Auth::logout();
	return Redirect::to('/');
});

Route::get('{id}/dashboard', array(
	'before' => 'auth',
	'as' => 'dashboard',
	'uses' => 'UsersController@dashboard'
	));


Route::post('users/login', array(
	'as' => 'login',
	'uses' => 'UsersController@login'
	));

Route::post('users/login_modal', array(
	'as' => 'login_modal',
	'uses' => 'UsersController@login_modal'
	));

Route::post('checkout_new_user', array(
	'as' => 'checkout',
	'uses' => 'UsersController@createFromCheckout'
));

Route::post('checkout_existing_user', array(
	'as' => 'checkout_existing',
	'uses' => 'UsersController@checkoutExisting'
	));
/**
 * Route to handle "Find Nearby" from the root 
 *
 * @return (return value of RestaurantsController@index)
 * @author Christian Cocking <cc@pith-studio.com>
 */
Route::get('restaurants', array(
	'before' => 'page',
	'as' => 'restaurants',
	'uses' => "RestaurantsController@index"
));

/**
 * Route to display the menu of a restaurant 
 *
 * @return (return value of RestaurantsController@show)
 * @author Christian Cocking <cc@pith-studio.com>
 */
Route::get('restaurants/{id}/menu', array(
	'as' => 'show_menu',
	'uses' => "RestaurantsController@show"
	));
// Route to test the add to bag form 
Route::post('addtobag', array(
	'as' => 'add_to_bag',
	'uses' => "RestaurantsController@addtobag"
	));
// Route to delete an item from the bag
Route::post('deleteitem', array(
	'as' => 'delete_item',
	'uses' => 'RestaurantsController@deleteitem'
	));

// Route to grab the cart to bag form 
Route::post('getcart', array(
	'as' => 'get_cart',
	'uses' => "RestaurantsController@getCart"
	));
// Route for ajax request for menu_item
Route::get('menuItemOptions', array(
	'as' => 'menu_item_options',
	'uses' => "RestaurantsController@menuItemOptions"
	));

Route::get('redis_json', array(
	'as' => 'redis_json',
	'uses' => "RestaurantsController@redisJson"
));


