<?php


class Restaurant extends Moloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'restaurants';

	/**
	 * Return all restaurants in the database.
	 *
	 * @todo Error codes for mishandled responses
	 * @return Eloquent Collecton Object
	 * @author Christian Cocking <cc@pith-studio.com>
 	 */
	public static function getAllRestaurants ($id=null)
	{	
		//Call to Laravel ORM built in to return all the restaurant objects
		$restaurants = Restaurant::all();
		return $restaurants;
	} 

	public static function applyFiltersToAllResults($filters=null,$page)
	{

		if (Session::has('search_longitude') && Session::has('search_longitude'))
		{
			$latitude = Session::get('search_latitude');
			$longitude = Session::get('search_longitude');
			$query = Restaurant::where('location_coordinates', 'near', array($longitude, $latitude));
			$restaurants = $query->get();

		} else
		{
			$restaurants = Restaurant::all();
		}

		
		if (!empty($filters))
		{
			foreach($restaurants as $key => $value) 
			{
				$unset_flag = true;
				foreach($filters as $filter_key => $filter_value)
				{
					if (strtolower($value->cuisine) == strtolower($filter_value))
					{
						$unset_flag = false;
					}
				}

				if ($unset_flag == true)
				{
					unset($restaurants[$key]);
				}
			}
		}

		$offset = ($page * 25) - 25;
		$rc = count($restaurants);
		$total_pages = ceil($rc / 5);
		$restaurants = $restaurants->toArray();
		$restaurants = array_slice($restaurants, $offset, 25);

		$data = Paginator::make($restaurants, $rc, 25);
		$data->setBaseUrl('restaurants');

		return $data;
	}

	public static function getRestaurantsByLocation ($longitude, $latitude)
	{
	
		$restaurants = Restaurant::where('location_coordinates', 'near', array($longitude, $latitude))->get();
		
		return $restaurants;
	}



	public static function getResaturantByID ($id=null)
	{

		$restaurant = Restaurant::find($id);
		return $restaurant;

	}

	public static function updateCart ($cart = null, $new_item_name, $new_item_price, $new_item_required, $new_item_optional, $new_item_si)
	{
		$new_cart_entry = array(
				$new_item_name => array(
					"price" => $new_item_price,
					"required_choices" => $new_item_required,
					"optional_choices" => $new_item_optional,
					"special_instruction" => $new_item_si
					)
			);
		
		if ($cart) {
			$cart[] = $new_cart_entry;
			return json_encode($cart);
		} else 
		{
			$new_cart = array($new_cart_entry);
			return json_encode($new_cart);
		}
	}

	public static function deleteFromCart ($cart, $index)
	{
		unset($cart[$index]);
		$cart = array_values($cart);
		return  json_encode($cart);
	}

	public static function cartSubTotal ($cart = null) 
	{	
		$total = 0;
		foreach($cart as $first_level => $items_container)
		{
		 	foreach ($items_container as $item_name => $items)
			{
			 foreach ($items as $item_fields => $field_value) 
			 {
				if ($item_fields == 'price') 
				{
					$total += $field_value;
				} else if ($item_fields == 'optional_choices') 
				{
				if(!empty($field_value))
					{
					foreach($field_value as $choice_name => $choice_price)
					{
					$total += $choice_price;
					}
				}
				}
			}
		}
	}
		return $total;
	}

	public static function cartTax ($sub_total)
	{
		return .165 * $sub_total;
	}
}
