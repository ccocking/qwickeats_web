<?php


class Menu extends Moloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'menus';

	public static function getAllMenus ($id=null)
	{	
		$menus = Menu::all();
		return $menus;
	} 

	public static function getMenuByID ($id=null)
	{	
		$menu = Menu::find($id);
		return $menu;
	} 

}
