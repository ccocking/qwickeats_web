<?php

class Transaction extends Moloquent {

	/**
	 * The database table used by the model.
	 *w
	 * @var string
	 */
	protected $table = 'transactions';


	public static function processTransaction ($user_id, $restaurant_id=null, $delivery_pickup, $payment_option, $total,$delivery_address, $subtotal, $tax)
	{

		$redis = Redis::connection();
		$redis_cart_key = Session::get('cart_id');
		$transaction = new Transaction;
		$transaction->user_id = $user_id;
		$transaction->restaurant_id = $restaurant_id;
		$transaction->restaurant_id = $restaurant_id;
		$transaction->delivery_pickup = $delivery_pickup;
		$transaction->payment_option = $payment_option;
		$transaction->subtotal = $subtotal;
		$transaction->delivery_address = $delivery_address;
		$transaction->tax = $tax;
		$transaction->total = $total;
		$transaction->order = json_decode($redis->get($redis_cart_key));
		$transaction->save();
		return $transaction;
	}

	public static function getUserTransactions($user_id)
	{
		$transactions = Transaction::where('user_id','=', $user_id)->get();
		return $transactions; 
	}

	public function user()
  {
  	return $this->belongsTo('User');
  }

	public function restaurant()
  {
  	return $this->belongsTo('Restaurant');
  }

}




