<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Moloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the token value for the "remember me" session.
	 *
	 * @return string
	 */
	public function getRememberToken()
	{
		return $this->remember_token;
	}

	/**
	 * Set the token value for the "remember me" session.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return 'remember_token';
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	public static function addDeliveryAddress($user_id, $delivery_address)
	{
		$user = User::find($user_id)->toArray();
		$push_flag = true;
		$saved_addresses = $user['delivery_addresses'];
		foreach ($saved_addresses as $index => $saved_address)
		{
			if (strtolower($saved_address['address_line1']) == strtolower($delivery_address['address_line1']))
			{
				if (strtolower($saved_address['address_line2']) == strtolower($delivery_address['address_line2']))
					{
						if (strtolower($saved_address['parish']) == strtolower($delivery_address['parish']))
						{
							$push_flag = false;
						}
					}	
			}
		}

		if ($push_flag)
		{
			//Geocode the Lon/Lat of the new address
			$coordinates = User::geoCodeAddress($delivery_address);
			$delivery_address['latitude'] = $coordinates['latitude'];
			$delivery_address['longitude'] = $coordinates['longitude'];
			array_push($saved_addresses, $delivery_address);
		}
		$user = User::find($user_id);

		$user->delivery_addresses = $saved_addresses;
		$user->save(); 

		return $user;

	}

	/**
	*
	*	Resolve longitude and latitude for the delivery address to be saved down
	*
	*/
	public static function createUser($input)
	{
		$user = new User;
		$delivery_address = $input['delivery_address'];
		$coordinates = User::geoCodeAddress($input['delivery_address']);
		$delivery_address['latitude'] = $coordinates['latitude'];
		$delivery_address['longitude'] = $coordinates['longitude'];


		$user->email = $input['email'];
		$user->password = Hash::make($input['password']);

		$user->delivery_addresses = [
			'0' => $delivery_address
		];
		//Create new user settings
		$user->account_settings = [
			'first_name' => $delivery_address['first_name'],
			'last_name' => $delivery_address['last_name'],
			'phone_number' => $delivery_address['phone_number']
		];
		$user->remember_token = $input['remember_token'];
		$user->save();
		return $user;
	}

	public static function geoCodeAddress($address)
	{
		$geocode = Geocoder::geocode($address['address_line1'] . ',' . $address['parish'] . 'Jamaica' );
		
		// Set the geocoder coordinates or based on the parish set the longitude/latitude
		if ($geocode)
		{
			$coordinates = array(
				'latitude' => $geocode['latitude'],
				'longitude' => $geocode['longitude']
				);
		} else
		{ // Based on parish set the lon/lat of the address
			switch ($address['parish']) 
			{
				case 'St. Ann':
					break;
				
				default:
					break;
			}

		}
		return $coordinates;
	}

	public static function getLonLat($address)
	{
		if (Auth::check())
		{
			$user = Auth::user();
			$delivery_addresses = $user->delivery_addresses;
			if ($delivery_addresses)
			{
				foreach ($delivery_addresses as $key => $value) 
				{
					if (strtolower($value['address_line1']) == strtolower($address))
					{
						$coordinates = array(
							'latitude' => $value['latitude'],
							'longitude' => $value['longitude']
							);

						return $coordinates;
					}
				}
			}
		}
		return false;
	}

	public static function updateSettings ($id, $ac)
	{
		$user = User::find($id);


		if (isset($ac['update_first_name']))
		{
			
			$updated_settings['first_name'] = $ac['update_first_name'];				
		} else
		{
			$updated_settings['first_name'] = $user->account_settings['first_name'];
		}

		if (isset($ac['update_last_name']))
		{
		 	$updated_settings['last_name'] = $ac['update_last_name'];
		}  else
		{
			$updated_settings['last_name'] = $user->account_settings['last_name'];
		}

		if (isset($ac['update_phone']))
		{
		 	$updated_settings['phone_number'] = $ac['update_phone'];
		}  else
		{
			$updated_settings['phone_number'] = $user->account_settings['phone_number'];
		}

		if ($updated_settings)
		{
			$user->account_settings = $updated_settings;
		}

		$user->save();

		if ($user)
		{
			return $user;
		} else
		{
			return false;
		}
	}


}
