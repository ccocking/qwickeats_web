<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// $this->call('UsersTableSeeder');
		// $this->command->info('Users table seeded!');

		// $this->call('MenusTableSeeder');
		// $this->command->info('Menus table seeded!');

		$this->call('RestaurantsTableSeederTwo');
		$this->command->info('Restaurants table seeded!');

		

		// $this->call('UserTableSeeder');
	}

}
