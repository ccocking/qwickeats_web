<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class RestaurantsTableSeederTwo extends Seeder {

	public function run()
	{

	  // create a faker instance
		$faker = Faker::create();

		$cuisines = 
		[
			"Chicken",
			"Chinese",
			"Sandwiches",
			"Burgers",
			"Jamaican",
			"Indian",
			"Salads"
		];
		$menus = Menu::all();
	  // generate random users
		foreach(range(0, 99) as $index)
		{

			//Generate the email address from first name
			$name = $faker->company;
			$email= $faker->email;
			$cuisine = $cuisines[rand(0,6)];
			$phone_number = $faker->phoneNumber;
			$location_coordinates = [$faker->randomFloat(7,-76.87,-76.72), $faker->randomFloat(7,18.07,17.90)];

			// $salt = Random::string(29);
			// $hash = Random::string(60);

			$address =
			[
				'line1' => $faker->streetAddress,
				'line2' => $faker->secondaryAddress,
				'parish'=> $faker->state,
				'apartment_number' => 11111,
				'special instructions' => "Turn off of Molynes Road",
			];

			$menu_id = $menus[$index]->_id;

			// $menu =
			// [
			// 	'categories' =>[
			// 		'breakfast' => 
			// 		[
			// 			'Ackee & Salt Fish' => 
			// 			[
			// 				'options' => 
			// 				['0'] 
			// 			]
			// 		],
			// 		'lunch' => 
			// 		[
			// 			'Fried Chicken' =>
			// 			[
			// 				'options' =>
			// 				['0']
			// 			]
			// 		],
			// 		'dinner' => 
			// 		[
			// 			'Bake Chicken' =>
			// 			[
			// 				'options' =>
			// 				['0']
			// 			]
			// 		]
			// 	]	
			// ];
			// create the user
			Restaurant::create(compact(
				'name',
				'email',
				'cuisine',
				'phone_number',
				'location_coordinates',
				'address',
				'menu_id',
				'created_at',
				'updated_at'
			));
		}
	}


}
