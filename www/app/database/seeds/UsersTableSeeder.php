<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder {

	public function run()
	{
		$neighborhoods = [
		"Cherry Gardens",
		"Norbrook",
		"UWI Mona",
		"UTech",
		"Jacks Hill",
		"Barbican",
		"Mona",
		"Havendale",
		"Smokey Vale",
		"Beverly Hills"
		];

	  // create a faker instance
		$faker = Faker::create();
	  // generate random users
		foreach(range(1, 20) as $index)
		{

			//Generate the email address from first name
			$email= $faker->email;
			$salt = Random::string(29);
			$hash = Random::string(60);
			$remember_token = "";

			$delivery_addresses =
			[
				'0' => 
				[ 'line1' => $faker->streetAddress,
					'line2' => $faker->secondaryAddress,
					'parish'=> $faker->state,
					'area' => $neighborhoods[rand(0,9)],
					'apartment_number' => 11111,
					'special instructions' => "Turn off of Molynes Road",
					'longitude' => $faker->longitude,
					'latitude' => $faker->latitude
				]
			];
			$credit_cards =
			[
				'0' =>
				[ 'number' => $faker->creditCardNumber,
					'expiration_month' => $faker->month,
					'expiration_year' => $faker->year,
					'card_type' => $faker->creditCardType,
					'billing_address' => 
					[ 
						'line1' => $faker->streetAddress,
						'line2' => $faker->secondaryAddress,
						'parish'=> $faker->state,
						'area' => $neighborhoods[rand(0,9)],
						'apartment_number' => 11111,
						'special instructions' => "Turn off of Molynes Road",
						'longitude' => $faker->longitude,
						'latitude' => $faker->latitude
					]
				]
			];
			// create the user
			User::create(compact(
				'email',
				'salt',
				'hash',
				'remember_token',
				'delivery_addresses',
				'credit_cards',
				'created_at',
				'updated_at',
				'timezone'
			));
		}
	}
}
