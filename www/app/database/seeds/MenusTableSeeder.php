<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class MenusTableSeeder extends Seeder {

	public function run()
	{
		foreach(range(1, 100) as $index) 
		{
		
		$categories_no_items = MenusTableSeeder::generateCategories();
		
		$categories = MenusTableSeeder::generateMenuItems($categories_no_items);

		// $menu =
		// [
		// 	'categories' => $categories
		// 	];


		Menu::create(compact(
			'categories'
			));
		}
	}


	public static function generateCategories ()
	{	
		//Create faker object
		$faker = Faker::create();
		// Collector for the categories
		$categories = [];
		//Random Number of categories to generate
		$num = rand (2,5);
		// Add random categories to the collector
		foreach(range(0, $num) as $index)
		{
			$categories[$index] = array(
				'name' => $faker->word,
				'menu_items' =>  ""
				);
			}
		//Return the array of generated categories
		return $categories;
	 }

	// Accepts an array of categories
	public static function generateMenuItems ($categories)
	{	
		$faker = Faker::create();

		foreach ($categories as $key => $value)
		{	
			$menuitems = [];
		  $num = rand (5,20);

		  foreach(range(0,$num) as $index) 
		  {
		  	$menuitems[$index] = array(
		  		'name'=> $faker->word,
		  		'price' => $faker->randomNumber(3),
		  		'description' => $faker->text(40),
		    	'menu_item_options' => ""
		  		);
		  }

		  $categories[$key]['menu_items'] = MenusTableSeeder::generateMenuItemOptions($menuitems);
			
		}
		return $categories;
	}

	public static function generateMenuItemOptions ($menuitems)
	{
		$faker = Faker::create();

		foreach ($menuitems as $key => $value)
		{
			//Collector for menu_item_options
			$menu_item_options = [];
			//Random Number of Options to Create
			$num = rand (0,4);

			foreach(range(0,$num) as $index)
			{
				$menu_item_options[$index] = array(
					'name' => $faker->word,
					'instructions' => $faker->text(12),
					'required' => $faker->boolean(50),
					'item_options' => ""
				);
			}
			$menuitems[$key]['menu_item_options'] = MenusTableSeeder::generateMenuItemOptionChoices($menu_item_options);
	}
		return $menuitems;
	}

	public static function generateMenuItemOptionChoices ($menu_item_options)
	{
		$faker = Faker::create();

		foreach ($menu_item_options as $key => $value)
		{
			//Collector for menu_item_option_choices
			$menu_item_option_choices = [];
			//Random Number of Choices to Create
			$num = rand (2,10);

			foreach (range(0, $num) as $index)
			{
				$menu_item_option_choices[$index] =  array(
					'name' => $faker->word,
					'price' => $faker->randomNumber(2),
				);
				// $menu_item_option_choices[$faker->word] = array(
	 			// 		 'price' => $faker->randomNumber(2)
				// 	);
			}
			$menu_item_options[$key]['item_options'] = $menu_item_option_choices;
		}		
		return $menu_item_options;
	}
	}
