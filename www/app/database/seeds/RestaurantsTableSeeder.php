<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class RestaurantsTableSeeder extends Seeder {

	public function run()
	{

	  // create a faker instance
		$faker = Faker::create();

		$cuisines = 
		[
			"Chicken",
			"Chinese",
			"Sandwiches",
			"Burgers",
			"Jamaican",
			"Indian",
			"Salads"
		];
	  // generate random users
		foreach(range(1, 100) as $index)
		{

			//Generate the email address from first name
			$name = $faker->company;
			$email= $faker->email;
			$cuisine = $cuisines[rand(0,6)];
			$phone_number = $faker->phoneNumber;
			$location_coordinates = [$faker->randomFloat(7,-76.87,-76.72), $faker->randomFloat(7,18.07,17.90)];
			// $salt = Random::string(29);
			// $hash = Random::string(60);

			$address =
			[
				'line1' => $faker->streetAddress,
				'line2' => $faker->secondaryAddress,
				'parish'=> $faker->state,
				'apartment_number' => 11111,
				'special instructions' => "Turn off of Molynes Road",
				
			];

			$categories = RestaurantsTableSeeder::generateCategories();
			$categories_with_items = RestaurantsTableSeeder::generateMenuItems($categories);

			$menu =
			[
				'categories' => $categories_with_items
				];

			// $menu =
			// [
			// 	'categories' =>[
			// 		'breakfast' => 
			// 		[
			// 			'Ackee & Salt Fish' => 
			// 			[
			// 				'options' => 
			// 				['0'] 
			// 			]
			// 		],
			// 		'lunch' => 
			// 		[
			// 			'Fried Chicken' =>
			// 			[
			// 				'options' =>
			// 				['0']
			// 			]
			// 		],
			// 		'dinner' => 
			// 		[
			// 			'Bake Chicken' =>
			// 			[
			// 				'options' =>
			// 				['0']
			// 			]
			// 		]
			// 	]	
			// ];
			// create the user
			Restaurant::create(compact(
				'name',
				'email',
				'cuisine',
				'phone_number',
				'location_coordinates',
				'address',
				'menu',
				'created_at',
				'updated_at'
			));
		}
	}

public static function generateCategories ()
{	
	//Create faker object
	$faker = Faker::create();
	// Collector for the categories
	$categories = [];
	//Random Number of categories to generate
	$num = rand (3,6);
	// Add random categories to the collector
	foreach(range(1, $num) as $index)
	{
		$categories[$faker->word] = "" ;
	}
	//Return the array of generated categories
	return $categories;
 }

// Accepts an array of categories
public static function generateMenuItems ($categories)
{	
	$faker = Faker::create();

	foreach ($categories as $key => $value)
	{
		$menuitems = [];
	  $num = rand (5,20);

	  foreach(range(1,$num) as $index) 
	  {
	  	$menuitems[$faker->word] = "" ;
	  }
	  $categories[$key] = RestaurantsTableSeeder::generateMenuItemOptions($menuitems);
	}
	return $categories;
}

public static function generateMenuItemOptions ($menuitems)
{
	$faker = Faker::create();

	foreach ($menuitems as $key => $value)
	{
		//Collector for menu_item_options
		$menu_item_options = [];
		//Random Number of Options to Create
		$num = rand (0,5);

		foreach(range(1,$num) as $index)
		{
			$menu_item_options[$faker->word] = "";
		}
		$menuitems[$key] = RestaurantsTableSeeder::generateMenuItemOptionChoices($menu_item_options);
	}
	return $menuitems;
}

public static function generateMenuItemOptionChoices ($menu_item_options)
{
	$faker = Faker::create();

	foreach ($menu_item_options as $key => $value)
	{
		//Collector for menu_item_option_choices
		$menu_item_option_choices = [];
		//Random Number of Choices to Create
		$num = rand (2,10);

		foreach (range(1, $num) as $index)
		{
			$menu_item_option_choices[$faker->word] = array(
 				 'price' => $faker->randomNumber(2)
				);
		}
		$menu_item_options["price"] = $faker->randomNumber(3);
		$menu_item_option_choices["instructions"] = $faker->text(12);
		$menu_item_option_choices["required"] = $faker->boolean(50);
		$menu_item_options[$key] = $menu_item_option_choices;
	}
	return $menu_item_options;
}
}
