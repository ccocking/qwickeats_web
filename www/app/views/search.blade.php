<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title> QwickEats - Order Food Online! </title>
		<meta name="description" content="">
		<meta name="author" content="">

		<!-- Use the correct meta names below for your web application
			 Ref: http://davidbcalhoun.com/2010/viewport-metatag 
			 
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">-->
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="smartadmin_css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="smartadmin_css/smartadmin-production.css">
		<link rel="stylesheet" type="text/css" media="screen" href="smartadmin_css/smartadmin-skins.css">

		<!-- SmartAdmin RTL Support is under construction
		<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.css"> -->

		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="css/your_style.css"> -->

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="css/demo.css">
		<link rel="stylesheet" type="text/css" media="screen" href="css/search.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="img/favicon/favicon.ico" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link href='http://fonts.googleapis.com/css?family=Playfair+Display+SC:400,700,900' rel='stylesheet' type='text/css'>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<header id="header">
			<!--<span id="logo"></span>-->

			<div id="logo-group">
				<span id="logo">
				<a href="/">
				<img src="/img/Logos/top_left.png"> 
				</a>
				</span>
				<!-- END AJAX-DROPDOWN -->
			</div>

			@if(Auth::check())

			<div class="btn-group" id="account_settings">
			  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" >
			    My Account <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu" role="menu">
			    <li><a href="users/{{{Auth::user()->_id}}}/account">Account Settings</a></li>
			    <li><a href="/order_history">Order History</a></li>
			    <li><a href="/saved_addresses">Saved Addresses</a></li>
			    <li><a href="/payment_info">Paymetn Info</a></li>
			  </ul>
		  </div>

			<span id="login-header-space"><a href="/logout" class="btn btn-danger">LOGOUT</a></span>
			@else
			<span id="login-header-space"><a data-target="#myModal" data-toggle="modal" class="btn btn-danger">LOGIN</a></span>
			@endif
		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<!-- Note: This width of the aside area can be adjusted through LESS variables -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as it --> 
					
					

							<h4 id="filter_results">Filter Results </h4>
						
	 
					
				</span>
			</div>
			<!-- end user info -->

			<!-- NAVIGATION : This navigation is also responsive

			To make this navigation dynamic please make sure to link the node
			(the reference to the nav > ul) after page load. Or the navigation
			will not initialize.
			-->
			<nav>
				<!-- NOTE: Notice the gaps after each icon usage <i></i>..
				Please note that these links work a bit different than
				traditional hre="" links. See documentation for details.
				-->

				<ul>
					<li>
						<a href="#" ><span class="menu-item-parent">Cuisines</span></a>
						<ul>
							<li>
							<label class="filter_dropdown_item" >
								<input type="checkbox" name="cuisine" value="burgers" id="burgers" onclick="filter_cuisine(this)" >Burgers</label>
							</li>
							<li>
							<label class="filter_dropdown_item" >
								<input type="checkbox" name="cuisine" value="chinese" id="chinese" onclick="filter_cuisine(this)" >Chinese</label>
							</li>
							<li>
							<label class="filter_dropdown_item" >
								<input type="checkbox" name="cuisine" value="indian" id="indian" onclick="filter_cuisine(this)" >Indian</label>
							</li>
							<li>
							<label class="filter_dropdown_item" >
								<input type="checkbox" name="cuisine" value="jamaican" id="jamaican" onclick="filter_cuisine(this)" >Jamaican</label>
							</li>
							<li>
							<label class="filter_dropdown_item" >
								<input type="checkbox" name="cuisine" value="salads" id="salads" onclick="filter_cuisine(this)" >Salads</label>
							</li>
							<li>
							<label class="filter_dropdown_item" >
								<input type="checkbox" name="cuisine" value="sandwiches" id="sandwiches" onclick="filter_cuisine(this)" >Sandwiches</label>
							</li>
						</ul>
					</li>
					<li>
						<a href="#" ><span class="menu-item-parent">Restaurant Price</span></a>
						<ul>
							<li>
							<label class="filter_dropdown_item" >
								<input type="checkbox" name="price" >$</label>
							</li>
							<li>
							<label class="filter_dropdown_item" >
								<input type="checkbox" name="price" >$$</label>
							</li>
							<li>
							<label class="filter_dropdown_item" >
								<input type="checkbox" name="price" >$$$</label>
							</li>
							<li>
							<label class="filter_dropdown_item" >
								<input type="checkbox" name="price" >$$$$</label>
							</li>
						</ul>
					</li>
					<li>
						<a href="#"> <span class="menu-item-parent">Delivery Minimum</span></a>
						<ul>
							<li class="filter_dropdown_item">
							<label  >
								<input type="checkbox" name="delivery_minimum"> <$500</label>
							</li>
							<li>
							<label class="filter_dropdown_item" >
								<input type="checkbox" name="delivery_minimum"> <$1000</label>
							</li>
							<li>
							<label class="filter_dropdown_item" >
								<input  type="checkbox" name="delivery_minimum"> <$2000</label>
							</li>
						</ul>
					</li>
					<li>
						<a href="#"><span class="menu-item-parent">Average Rating</span></a>
						<ul>
							<li>
							<label class="filter_dropdown_item" >
								<input class="filter_dropdown_item" type="checkbox" name="rating"> > 3 Star</label>
							</li>
							<li>
							<label class="filter_dropdown_item" >
								<input class="filter_dropdown_item" type="checkbox" name="rating"> > 4 Star</label>
							</li>
							<li>
							<label class="filter_dropdown_item" >
								<input class="filter_dropdown_item" type="checkbox" name="rating"> > 5 Star</label>
							</li>
						</ul>
					</li>
				</ul>
			</nav>

		</aside>
		<!-- END NAVIGATION -->

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

			
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li class="aligncenter" id="search_title">
						Restaurant Results
					</li>
				</ol>
				<!-- end breadcrumb -->

				<!-- You can also add more buttons to the
				ribbon for further usability

				Example below:

				<span class="ribbon-button-alignment pull-right">
				<span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
				<span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
				<span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
				</span> -->

			</div>
			<!-- END RIBBON -->
			<!-- Render the Restaurant Lists here -->
			<!-- MAIN CONTENT -->
			<div id="content" style="opacity:1" i>
		@foreach ($restaurants as $restaurant)
				<div class="row" id="restaurants_list_item">
					<div class="col-sm-12">
						<div class="well clearfix">
					              <a class="pull-left" href="#">
					                <img class="media-object" src="http://s1.seamless.com/-/ri/vl/11898/93/60?fs=1">
					              </a>
					              	<div class="pull-right aligncenter" href="#">
					              		{{ HTML::link("restaurants/{$restaurant['_id']}/menu", 'View Menu', array("id" => "view_menu", "class" => "btn btn-danger pull-right"))}}
					              	</div>
					              <div class="media-body">
					                <h3 class="media-heading" id="restaurant_title">{{ $restaurant['name'] }}</h3>
					                <ul >
					                	<li><i class="glyphicon glyphicon-time pull-left"></i> <span id="restaurant_info">Opening Hours:</span></li>
					                	<li><i class="glyphicon glyphicon-flag pull-left"></i> <span id="restaurant_info">{{ $restaurant['phone_number'] }}</span></li>
					                	<li><i class="glyphicon glyphicon-flag pull-left"></i> <span id="restaurant_info">{{ $restaurant['address']['line1'] }}</span></li>
					                	<li><i class="glyphicon glyphicon-fire pull-left"></i> <span id="restaurant_info"> {{ $restaurant['cuisine']}}</span></li>
					                </ul>
					              </div>
						</div>
					</div>
				</div>
		@endforeach
		{{ $restaurants->links()}}
				</div>


			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->
		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		      <div class="modal-body" id="menu_item_body">
							<form action="users/login_modal" method="POST"id="login-form" class="smart-form client-form">
								<header>
									Log In! 

								</header>

								<fieldset>
									
									<section>
										<label class="label">E-mail</label>
										<label class="input"> <i class="icon-append fa fa-user"></i>
											<input type="email" name="email">
											<b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter email address</b></label>
									</section>

									<section>
										<label class="label">Password</label>
										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input type="password" name="password">
											<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
										<div class="note">
											<a href="forgotpassword.html">Forgot password?</a>
										</div>
									</section>

									<section>
										<label class="checkbox">
											<input type="checkbox" name="remember" checked="">
											<i></i>Stay signed in</label>
									</section>
								</fieldset>
								<footer>
									<button type="submit" class="btn btn-danger">
										Sign in
									</button>
								</footer>
							</form>		      
						</div>

		  </div>
		</div>






		<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="js/libs/jquery-2.0.2.min.js"><\/script>');
			}

			function filter_cuisine (cb) 
			{
				var checkboxValues = {};
				$(":checkbox").each(function(){
				  checkboxValues[this.id] = this.checked;
				});
				$.cookie('checkboxValues', checkboxValues, { expires: 1, path: '/' })

	  		if(cb.checked == false)
	  		{
	  			// They are unchecking remove from the filter

	  			$.get("/restaurants/remove_filter",
	  				 		{
	  				 			filter: cb.value
	  				 		}).done(function(result) {
	  				 			$('#content').empty().append(result);
	  				 		});
	  			
	  		} else
	  		{

	  			$.get("/restaurants/apply_filter",
	  				 	{
	  				 		filter: cb.value
	  				 	}).done(function(result) {
	  				 		$('#content').empty().append(result);
	  				 		console.log('append');
	  				 	});
	  		}
			};


			function repopulateCheckboxes(){
			  var checkboxValues = $.cookie('checkboxValues');
			  if(checkboxValues){
			    Object.keys(checkboxValues).forEach(function(element) {
			      var checked = checkboxValues[element];
			      $("#" + element).prop('checked', checked);
			    });
			  }
			};
		</script>

		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events
		<script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->
		<!-- BOOTSTRAP JS -->
		<script src="js/bootstrap/bootstrap.min.js"></script>	
		<!-- MAIN APP JS FILE -->
		<script src="js/app.js"></script>

		<script type="text/javascript">
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		$(document).ready(function() 
		{
			$.cookie.json = true;
			repopulateCheckboxes();

			var theForm = document.getElementById("login-form");
			console.log(theForm);
			var input = document.createElement('input');
			input.type = 'hidden';
			input.name = 'location';
			input.value = window.location.pathname + window.location.search;
			theForm.appendChild(input); 
		});
		</script>
	</body>
</html>
