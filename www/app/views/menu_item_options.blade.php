<div class="modal-dialog">
  <div class="modal-content">
  {{Form::open(["action" => "add_to_bag","method" => "post","id" => "menu_item_details"])}}
    {{Form::hidden('item_added',$menu_item_input)}}
    {{Form::hidden('item_base_price', $item_price)}}
    {{Form::hidden('quantity', 1)}}
    {{Form::hidden('restaurant_id', $restaurant_id)}}
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title" id="menu_item_title"> {{$menu_item_input}} <span id="modal_header_price">${{$item_price}}</span></h4>
    </div>
    <div class="modal-body" id="menu_item_body">
      @foreach ($menu_item_options as $index_menu => $menu_item_option )
      <span id = "hide">{{ $opt_option_name = $menu_item_option['name'] }} </span>
      <div id="menu_item_modal" class="col-md-12">{{$menu_item_option['name']}}<span id="option_instructions"> {{$menu_item_option['instructions']}} </span></div>
      <div id="menu_item_choices" class="row">
        @foreach ($menu_item_option['item_options'] as $index => $item_option)
        <div id="menu_item_option" class="col-md-4">
          @if($menu_item_option["required"] == 1)
          {{ Form::radio("required[$opt_option_name]",$item_option['name'],1)}} {{$item_option['name']}} 
          @else
          <span id ="hide">{{ $option_name = $item_option['name'] }} </span>
          {{ Form::checkbox("optional_items[$option_name]",$item_option['price'])}} {{$item_option['name']}}
          <span id="choice_price">${{$item_option['price']}}</span>
          @endif
        </div>
        @endforeach
      </div>
      @endforeach
      <br>
      <div>
        <span id="spec_instructions_title">Special Instructions</span>
        <span id="spec_instructions_prompt"> (eg. "Hot sauce in the bag" ) </span>
        <textarea class="form-control" id="special_instructions_content" rows="3"></textarea>
      </div>
      <div class="modal-footer">
        <button id="close_modal" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        {{HTML::link("#","Add To Bag",["id" => "save_to_bag", "class"=>"btn btn-danger"])}}
      </div>
    </div>
  </div>
  {{Form::close()}}
</div>

<script>

$("#save_to_bag").click(function()
{

  var data = $( "#menu_item_details" ).serialize();

  var url = $("#menu_item_details").attr( "action" );
  
  console.log(url);
  // Send the data using post
  var posting = $.post( url, data );
  
  posting.done(function( data ) {
      $('#myModal').modal('hide');
      $('#bag_details_container').empty().append(data);
  });
});

// Attach a submit handler to the form
// function formSubmit(e) {
  
//   e.preventDefault();  // Stop form from submitting normally
//   var $inputs = $('#menu_item_details :input:checked');
//   var $inputs2 = $('#menu_item_details').find(':hidden');
//   var textarea_text = $('#special_instructions_content').val();


//   var values = {};
//   $inputs.each(function() {
//         values[this.name] = $(this).val();
//     });
//   $inputs2.each(function() {
//         values[this.name] = $(this).val();
//     });

//   values['special_instruction'] = textarea_text;
//   var $form = $( this ),
//     url = $form.attr( "action" );
 
  
//   // Send the data using post
//   var posting = $.post( url, values );
 
//   posting.done(function( data ) {
//       $('#myModal').modal('hide');
     
//       $('#bag_details_container').empty().append(data);
//     );
//   });
//   return false;
// };
</script>
