<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title> QwickEats - Order Food Online! </title>
		<meta name="description" content="">
		<meta name="author" content="">

		<!-- Use the correct meta names below for your web application
			 Ref: http://davidbcalhoun.com/2010/viewport-metatag 
			 
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">-->
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="/smartadmin_css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="/smartadmin_css/smartadmin-production.css">
		<link rel="stylesheet" type="text/css" media="screen" href="/smartadmin_css/smartadmin-skins.css">

		<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="/css/dashboard.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="img/favicon/favicon.ico" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link href='http://fonts.googleapis.com/css?family=Playfair+Display+SC:400,700,900' rel='stylesheet' type='text/css'>
		
		<script type="text/javascript">
		var place;
		var autocomplete;
			function initialize() {
			  // Create the autocomplete object, restricting the search
			  // to geographical location types.
			  autocomplete = new google.maps.places.Autocomplete(
			      /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
			    	{ types: ['geocode'],
			    		componentRestrictions: {country: "jm"}
			    	 });
			  // When the user selects an address from the dropdown,
			  // populate the address fields in the form.
			  google.maps.event.addListener(autocomplete, 'place_changed', function() {
			  	place = autocomplete.getPlace();
			  	
			  	//place = autocomplete.getPlace().geometry;
			  });
			}

			function geolocate() {
			  if (navigator.geolocation) {
			    navigator.geolocation.getCurrentPosition(function(position) {
			      var geolocation = new google.maps.LatLng(
			          18.021997, -76.777782);
			      autocomplete.setBounds(new google.maps.LatLngBounds(geolocation,
			          geolocation));
			    });
			  }
			}

				function mySubmit() 
				{
				 var newlatlong = new google.maps.LatLng(place.geometry.location.lat(),place.geometry.location.lng());
		     document.getElementById('hiddenField').value = JSON.stringify(newlatlong); 
		     document.getElementById("enter_address").submit();
			  }
			</script>


	</head>
	<body onload="initialize()">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<header id="header">
			<!--<span id="logo"></span>-->

			<div id="logo-group">
				<span id="logo">
				<a href="/">
				<img src="/img/Logos/top_left.png"> 
				</a>
				</span>
				<!-- END AJAX-DROPDOWN -->
			</div>

			@if(Auth::check())

		<div class="btn-group" id="account_settings">
		  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" >
		    My Account <span class="caret"></span>
		  </button>
		  <ul class="dropdown-menu" role="menu">
		    <li><a href="/users/{{{Auth::user()->_id}}}/account">Account Settings</a></li>
		    <li><a href="/order_history">Order History</a></li>
		    <li><a href="/saved_addresses">Saved Addresses</a></li>
		    <li><a href="/payment_info">Paymetn Info</a></li>
		  </ul>
	  </div>

			<span id="login-header-space"><a href="/logout" class="btn btn-danger">LOGOUT</a></span>
			@else 
			<span id="login-header-space"><a href="/login_modal" class="btn btn-danger">LOGIN</a></span>
			@endif
		</header>


			<!-- RIBBON -->
			<div id="ribbon">
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li class="aligncenter" id="menu_title">
						User Dashboard
					</li>
				</ol>
			</div>
			<h1>Search For Food</h1>
			<div class="row" id="search_bar_container">
				<div class="col-xs-12 col-sm-12 col-md-12">
						<form action="/restaurants" id="choose_area" class="form-inline" role="form">
									<span class="form-group row" id="select_neighborhood_form">
									{{ Form::select("location",array
									(
										"Norbrook",
										"Jacks Hill",
										"Cherry Gardens",
										"UWI Mona",
										"UTech",
										"Mona",
										"Barbican"),null,['class'=>'fa input-xlarge form-control','id' => 'select_neighborhood']) }}
									</span>
									<button id="enter_neighborhood_button" type="submit" class="btn btn-danger">Search Near Neighborhood</button>
						</form>	
				</div>

				@if(isset($saved_addresses))
				<div class="col-xs-12 col-sm-12 col-md-12">
						<form action="/restaurants" id="choose_area" class="form-inline" role="form">
									<span class="form-group row" id="select_saved_address_form">
									{{ Form::select("saved_address",$saved_addresses,null,['class'=>'fa input-xlarge form-control','id' => 'select_saved_address']) }}
									</span>
									<button id="enter_neighborhood_button" type="submit" class="btn btn-danger">Search Saved Address</button>
						</form>	
				</div>
				@endif

				<div class="col-xs-12 col-sm-12 col-md-12">
						<form action="/restaurants" id="enter_address" class="form-inline" role="form">
								<span class="form-group row" id="enter_address_form">
									<input id="autocomplete" placeholder="Enter your address" onFocus="geolocate()" type="text" class="form-control input-xlarge">
									<input type='hidden' id= 'hiddenField' name='location' value='' />
								</span>	
									<button onclick="mySubmit()" id="enter_neighborhood_button" type="submit" class="btn btn-danger"> Find Food Near Address </button>
						</form>		
				</div>	
				
			</div>
			<h1>Order History</h1>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<div id="table_container" class="well clearfix">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th>Restaurant</th>
									<th>Order Details</th>
									<th>Reorder?</th>
								</tr>
							</thead>
							<tbody>
							@if(!empty($transactions))
								@foreach($transactions as $key => $value)
									<tr>
										<td align="center">{{$value->restaurant->name}}</td>
										<td align="center"> 
												 {{$value->delivery_pickup}} | {{$value->total}} <br>
												 {{$value->delivery_address['address_line1']}} <br>
												 {{date_format(date_create($value->created_at),"Y-m-d H:i") }}
										 </td>
										<td align="center"> {{ HTML::link("$", 'ReOrder', array("id" => "view_menu", "class" => "btn btn-danger pull-right"))}} </td>
									</tr>
								@endforeach
							@endif
							</tbody>
						</table>
					</div>
				</div>
			</div>	
		<script>
		var enter_clicks = 0;

		$('#autocomplete').keypress(function(e) {
			if (enter_clicks == 1)
			{
				return true;
			}

		  if (e.which == 13) {
		  	enter_clicks++;
		    google.maps.event.trigger(autocomplete, 'place_changed');
		    return false;
		  }
		});
		</script>

		<script src="/js/bootstrap/bootstrap.min.js"></script>	

	</body>
</html>
