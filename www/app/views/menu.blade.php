<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title> QwickEats - Order Food Online! </title>
		<meta name="description" content="">
		<meta name="author" content="">

		<!-- Use the correct meta names below for your web application
			 Ref: http://davidbcalhoun.com/2010/viewport-metatag 
			 
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">-->
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="/smartadmin_css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="/smartadmin_css/smartadmin-production.css">
		<link rel="stylesheet" type="text/css" media="screen" href="/smartadmin_css/smartadmin-skins.css">

		<!-- SmartAdmin RTL Support is under construction
		<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.css"> -->

		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="css/your_style.css"> -->

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="/css/demo.css">
		<link rel="stylesheet" type="text/css" media="screen" href="/css/menu.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="img/favicon/favicon.ico" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link href='http://fonts.googleapis.com/css?family=Playfair+Display+SC:400,700,900' rel='stylesheet' type='text/css'>
	</head>
	<body class="">
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<!-- HEADER -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as it --> 
							<h4 id="filter_results"> Order Information </h4>
				</span>
			</div>
			<nav id="bag_details_container">

			</nav>
		</aside>
		<header id="header">
			<!--<span id="logo"></span>-->

			<div id="logo-group">
				<span id="logo"> 
				<a href="/">
				<img src="/img/Logos/top_left.png"> 
				</a>
				</span>

				<!-- END AJAX-DROPDOWN -->
			</div>

			@if(Auth::check())

				<div class="btn-group" id="account_settings">
				  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" >
				    My Account <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu" role="menu">
				    <li><a href="/users/{{{Auth::user()->_id}}}/account">Account Settings</a></li>
				    <li><a href="/order_history">Order History</a></li>
				    <li><a href="/saved_addresses">Saved Addresses</a></li>
				    <li><a href="/payment_info">Paymetn Info</a></li>
				  </ul>
			  </div>


			<span id="login-header-space"><a href="/logout" class="btn btn-danger">LOGOUT</a></span>
			@else 
			<span id="login-header-space"><a data-target="#myModalLogin" data-toggle="modal" class="btn btn-danger">LOGIN</a></span>
			@endif
		</header>


		<!-- MAIN PANEL -->

		<div id="main" role="main">


			<!-- RIBBON -->
			<div id="ribbon" data-restaurant={{$restaurant->_id}}>

			
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li class="aligncenter" id="menu_title">
						{{ $restaurant->name}} 
					</li>
				</ol>
			</div>

				<!-- end breadcrumb -->

				<!-- You can also add more buttons to the
				ribbon for further usability

				Example below:

				<span class="ribbon-button-alignment pull-right">
				<span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
				<span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
				<span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
				</span> -->


				<div id="content" style="opacity:1">
				@foreach ($menu_categories as $category_index => $category)
					

				<div class="well clearfix">
				<div class="row" id="restaurants_list_item">
					<div id="menu_category" class="col-sm-12">
						{{ $category['name'] }}
					</div>
					<div class="row">
						@foreach ($category['menu_items'] as $menu_item_index => $menu_item)
					  <div id="menu_item" class="col-md-3">{{ HTML::link('#',$menu_item['name'], array("data-target" => "#myModal", "data-toggle" => "modal", "id" => "menu_item_link", "data-restaurant" => $restaurant->_id, "data-menucat" => $category['name'], "data-menuitem" => $menu_item['name']))}}</div>
					 	<div id="menu_price" class="col-md-3"> ${{$menu_item['price']}}</div>
					  @endforeach
					  </div>
					  </div>
				</div>
				@endforeach
				
				<!-- Modal -->
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				        <h4 class="modal-title" id="menu_item_title"> <span id="modal_header_price"></span></h4>
				      </div>
				      <div class="modal-body" id="menu_item_body">
				        <i class="fa fa-gear fa-4x fa-spin"></i>
				      </div>
				      <div class="modal-footer">
				        <button id="close_modal" type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				        <button id="save_change_modal"type="button" class="btn btn-danger">Save to Bag</button>
				      </div>
				    </div>
				  </div>
				</div>
			</div>

			<div class="modal fade" id="myModalLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			      <div class="modal-body" id="menu_item_body">
								<form action="/users/login_modal" method="POST"id="login-form" class="smart-form client-form">
									<header>
										Log In! 

									</header>

									<fieldset>
										
										<section>
											<label class="label">E-mail</label>
											<label class="input"> <i class="icon-append fa fa-user"></i>
												<input type="email" name="email">
												<b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter email address</b></label>
										</section>

										<section>
											<label class="label">Password</label>
											<label class="input"> <i class="icon-append fa fa-lock"></i>
												<input type="password" name="password">
												<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
											<div class="note">
												<a href="forgotpassword.html">Forgot password?</a>
											</div>
										</section>

										<section>
											<label class="checkbox">
												<input type="checkbox" name="remember" checked="">
												<i></i>Stay signed in</label>
										</section>
									</fieldset>
									<footer>
										<button type="submit" class="btn btn-danger">
											Sign in
										</button>
									</footer>
								</form>		      
							</div>
			  </div>
			</div>


			<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
			<script>
				if (!window.jQuery) {
					document.write('<script src="js/libs/jquery-2.0.2.min.js"><\/script>');
				}
			</script>

			<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
			<script>
				if (!window.jQuery.ui) {
					document.write('<script src="js/libs/jquery-ui-1.10.3.min.js"><\/script>');
				}
			</script>
	

			<!-- BOOTSTRAP JS -->
			<script src="/js/bootstrap/bootstrap.min.js"></script>

			
			<!-- MAIN APP JS FILE -->
			<script src="/js/app.js"></script>

			

			<script type="text/javascript">
			
			// DO NOT REMOVE : GLOBAL FUNCTIONS!
			


			$(document).ready(function() {

				var theForm = document.getElementById("login-form");
				console.log(theForm);
				var input = document.createElement('input');
				input.type = 'hidden';
				input.name = 'location';
				input.value = window.location.pathname + window.location.search;
				theForm.appendChild(input); 


				var restaurantID = $('#ribbon').data('restaurant');
				//pageSetUp();
  			var posting = $.post("/getcart", 
  			{
  				restaurant_id : restaurantID
  			});

  			posting.done(function(result) 
  			{
      	$('#bag_details_container').empty().append(result);
  			}
  			);
			 });

			$(document).on("click", "#menu_item_link", function () {
			     var restaurantID = $(this).data('restaurant');
			     var menu_category = $(this).data('menucat');
			     var menu_item = $(this).data('menuitem');
			     var data = 
			     {
			     	"restaurant_id" : restaurantID,
			     	"menu_category" : menu_category,
			     	"menu_item" : menu_item
			     };

			     $.ajax("/menuItemOptions",
			     	{	
			     		data: data
			     	}).done(function (result) {
			     	$('#menu_item_title').empty().append(menu_item);
    				$('#myModal').empty().append(result);
						});
			   });

			</script>
	</body>
</html>
