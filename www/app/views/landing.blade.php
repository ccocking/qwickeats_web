<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title> QwickEats - Order Food Online! </title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->

		{{ HTML::style("bootstrap/css/bootstrap.min.css")}}
		{{ HTML::style("smartadmin_css/font-awesome.min.css")}}

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		{{ HTML::style("smartadmin_css/smartadmin-production.css")}}
		{{ HTML::style("smartadmin_css/smartadmin-skins.css")}}
		{{ HTML::style("css/demo.css")}}


		<!-- FAVICONS -->
		<link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="img/favicon/favicon.ico" type="image/x-icon">
		<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<!-- GOOGLE FONT -->
		<link href='http://fonts.googleapis.com/css?family=Playfair+Display+SC:400,700,900' rel='stylesheet' type='text/css'>
		

		<script type="text/javascript">
		var place;
		var autocomplete;
			function initialize() {
			  // Create the autocomplete object, restricting the search
			  // to geographical location types.
			  autocomplete = new google.maps.places.Autocomplete(
			      /** @type {HTMLInputElement} */(document.getElementById('autocomplete')),
			    	{ types: ['geocode'],
			    		componentRestrictions: {country: "jm"}
			    	});
			  // When the user selects an address from the dropdown,
			  // populate the address fields in the form.
			  google.maps.event.addListener(autocomplete, 'place_changed', function() {
			  	place = autocomplete.getPlace();
			  	
			  	//place = autocomplete.getPlace().geometry;
			  });
			}

		
			function geolocate() {
			  if (navigator.geolocation) {
			    navigator.geolocation.getCurrentPosition(function(position) {
			      var geolocation = new google.maps.LatLng(
			          18.021997, -76.777782);
			      autocomplete.setBounds(new google.maps.LatLngBounds(geolocation,
			          geolocation));
			    });
			  }
			}

			function mySubmit() 
			{
			 var newlatlong = new google.maps.LatLng(place.geometry.location.lat(),place.geometry.location.lng());
	     document.getElementById('hiddenField').value = JSON.stringify(newlatlong); 
	     document.getElementById("enter_address").submit();
		  }
		</script>

	</head>

	<body id="login" class="animated fadeInDown" onload="initialize()"> <!-- animated fadeInDown animates how the entire page is rendered -->
		<img src="img/landing2.jpg" class="bg">
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
		<header id="header">
			<!--<span id="logo"></span>-->

			<div id="logo-group">
				<span id="logo"> QwickEats </span>

				<!-- END AJAX-DROPDOWN -->
			</div>

			<span id="login-header-space"><a href="" class="btn btn-danger">Need Help?</a></span>

		</header>

		<div id="main" role="main">

			<!-- MAIN CONTENT -->
			<div id="content" class="container">
				<div id="locationField">
				 </div>

				<div class="row">
									<h2>Hungry? Search for food near you! </h2>
					<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 ">
					<div class="pull-left login-desc-box-l">
						<form action="restaurants" id="choose_area" class="form-inline" role="form">
									<div class="form-group row" >
									  <div class="col-lg-2">

									{{ Form::select("user_location",array
									(
										"Norbrook",
										"Jacks Hill",
										"Cherry Gardens",
										"UWI Mona",
										"UTech",
										"Mona",
										"Barbican"),null,['class'=>'fa input-xxlarge form-control','id' => 'select_neighborhood']) }}
									
										</div>
									</div>
									<button id="enter_neighborhood_button" type="submit" class="fa btn btn-danger">Find Food Near This Neighborhood</button>
						</form>	
						<form action="restaurants" id="enter_address" class="form-inline" role="form">
								<div class="form-group row" >
									<input id="autocomplete" placeholder="Enter your address" onFocus="geolocate()" type="text" class="form-control input-md ">
									<input type='hidden' id= 'hiddenField' name='location' value='' />
								</div>	
									<button onclick="mySubmit()" id="enter_address_button" type="submit" class="fa btn btn-danger">Find Food Near This Address</button>
						</form>			
						</div>
						
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<div class="well no-padding">
							<form action="users/login" method="POST"id="login-form" class="smart-form client-form">
								<header>
									Existing Customer? Log In! 
								</header>
								<fieldset>
									
									<section>
										<label class="label">E-mail</label>
										<label class="input"> <i class="icon-append fa fa-user"></i>
											<input type="email" name="email">
											<b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter email address</b></label>
									</section>

									<section>
										<label class="label">Password</label>
										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input type="password" name="password">
											<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
										<div class="note">
											<a href="forgotpassword.html">Forgot password?</a>
										</div>
									</section>

									<section>
										<label class="checkbox">
											<input type="checkbox" name="remember" checked="">
											<i></i>Stay signed in</label>
									</section>
								</fieldset>
								<footer>
									<button type="submit" class="btn btn-danger">
										Sign in
									</button>
								</footer>
							</form>
						</div>	
					</div>
				</div>
			</div>
		</div>
		<script>
		var enter_clicks = 0;

		$('#autocomplete').keypress(function(e) {
			if (enter_clicks == 1)
			{
				return true;
			}

		  if (e.which == 13) {
		  	enter_clicks++;
		    google.maps.event.trigger(autocomplete, 'place_changed');
		    return false;
		  }
		});
		</script>

		<script src="/js/bootstrap/bootstrap.min.js"></script>	





	</body>
	</html>
