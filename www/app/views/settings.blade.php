<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title> QwickEats - Order Food Online! </title>
		<meta name="description" content="">
		<meta name="author" content="">

		<!-- Use the correct meta names below for your web application
			 Ref: http://davidbcalhoun.com/2010/viewport-metatag 
			 
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">-->
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="/smartadmin_css/font-awesome.min.css">

		
		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="/smartadmin_css/smartadmin-production.css">
		<link rel="stylesheet" type="text/css" media="screen" href="/smartadmin_css/smartadmin-skins.css">

		<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="/css/settings.css">

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="img/favicon/favicon.ico" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link href='http://fonts.googleapis.com/css?family=Playfair+Display+SC:400,700,900' rel='stylesheet' type='text/css'>
		

	</head>
	<body>
		<!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

		<header id="header">
			<!--<span id="logo"></span>-->

			<div id="logo-group">
				<span id="logo">
				<a href="/">
				<img src="/img/Logos/top_left.png"> 
				</a>
				</span>
				<!-- END AJAX-DROPDOWN -->
			</div>

			@if(Auth::check())
			<span id="login-header-space"><a href="/logout" class="btn btn-danger">LOGOUT</a></span>

			<span id="login-header-space"><a href="/" class="btn btn-danger">PLACE AN ORDER</a></span>
			@else 
			<span id="login-header-space"><a href="/login_modal" class="btn btn-danger">LOGIN</a></span>
			@endif
		</header>
			<!-- RIBBON -->
			<div id="ribbon">
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li class="aligncenter" id="menu_title">
						Account Settings
					</li>
				</ol>
			</div>

		<!-- Nav tabs -->
		<ul class="nav nav-tabs">
		  <li id="account_settings_tab"><a href="#account_settings_tab_pane" data-toggle="tab">Account Settings</a></li>
		  <li id="order_history_tab"><a href="#order_history_tab_pane" data-toggle="tab">Order History</a></li>
		  <li id="saved_addresses_tab"><a href="#saved_addresses_tab_pane" data-toggle="tab">Saved Addresses</a></li>
		  <li id="payment_info_tab"><a href="#payment_info_tab_pane" data-toggle="tab">Payment Info</a></li>
		</ul>

		<!-- Tab panes -->
		
		{{Form::open(array('action' => 'update_account_settings'))}}
		<div class="tab-content">
		  <div class="tab-pane active" id="account_settings_tab_pane">
		  <h2>{{Auth::user()->email}}</h2>
		  <div role="form">
		    
		  <!-- Text input-->
		  <div class="form-group">
		    <label class="col-md-2 control-label" for="update_first_name">Update First Name</label>  
		    <div class="col-md-4">
		    {{Form::input('text','update_first_name',Auth::user()->account_settings['first_name'],array('class' =>'form-control input-sm','id' => 'update_first_name'))}}
		    </div>
		  </div>

		  <!-- Text input-->
		  <div class="form-group">
		    <label class="col-md-2 control-label" for="update_last_name">Update Last Name</label>  
		    <div class="col-md-4">
		    {{Form::input('text','update_last_name',Auth::user()->account_settings['last_name'],array('class' =>'form-control input-sm','id' => 'update_last_name'))}} 
		    </div>
		  </div>

		 <div class="form-group">
		   <label class="col-md-2 control-label" for="update_password">Update Password</label>  
		   <div class="col-md-4">
		    {{Form::input('password','update_password',null,array('placeholder' => 'enter password','class' =>'form-control input-sm','id' => 'update_password'))}} 
		   </div>
		 </div>


		  <!-- Text input-->
		  <div class="form-group">
		    <label class="col-md-2 control-label" for="confirm_password">Confirm Password</label>  
		    <div class="col-md-4">
		    {{Form::input('password','confirm_password',null,array('placeholder' => 'retype password','class' =>'form-control input-sm','id' => 'confirm_password'))}} 
		    </div>
		  </div>

		  <!-- Text input-->
		  <div class="form-group">
		    <label class="col-md-2 control-label" for="update_phone">Update Phone Number</label>  
		    <div class="col-md-4">
		    {{Form::input('text','update_phone',Auth::user()->account_settings['phone_number'],array('placeholder' => 'Enter Phone Number','class' =>'form-control input-sm','id' => 'update_phone'))}} 
		    </div>
		  </div>
		  </div>
		  {{Form::submit('Update Account Settings',array())}}

		  </div>

		  <div class="tab-pane" id="order_history_tab_pane">
		  	<div class="row">
		  		<div class="col-xs-12 col-sm-6 col-md-12">
		  			<div id="table_container" class="well clearfix">
		  				<table class="table table-bordered">
		  					<thead>
		  						<tr>
		  						<th> Order Placed </th>
		  						<th> Restaurant </th>
		  						<th> Order # </th>
		  						<th> Total </th>
		  						<th> Destination </th>
		  						</tr>
		  					</thead>
		  					<tbody>
		  					@if(!empty($transactions))
		  						@foreach($transactions as $key => $value)
		  							<tr>
		  								<td align="center">{{date_format(date_create($value->created_at),"Y-m-d H:i") }}</td>
		  								<td align="center"> {{$value->restaurant->name}} </td>
		  								<td align="center"> 99999999 </td>
		  								<td align="center"> {{$value->total}} </td>
		  								<td align="center"> {{$value->delivery_address['address_line1']}} </td>
		  							</tr>
		  						@endforeach
		  					@endif
		  					</tbody>
		  				</table>
		  			</div>
		  		</div>
		  	</div>	
		  </div>

		  <div class="tab-pane" id="saved_addresses_tab_pane">
		  	<div class="row">
		  		<div class="col-xs-12 col-sm-6 col-md-12">
		  			<div id="table_container" class="well clearfix">
		  				<table class="table table-bordered">
		  					<thead>
		  						<tr>
		  						<th> Label </th>
		  						<th> Address </th>
		  						<th> Delivery Instructions </th>
		  						<th> Actions </th>
		  						</tr>
		  					</thead>
		  					<tbody>
		  					@if(!empty($transactions))
		  						@foreach($transactions as $key => $value)
		  							<tr>
			  							<td align="center"> (label) </td>
			  							<td align="center"> {{$value->delivery_address['address_line1']}} <br>
			  																	{{$value->delivery_address['address_line2']}}
			  							</td>
			  							<td align="center"> </td>
			  							<td align="center"> - </td>
		  							</tr>
		  						@endforeach
		  					@endif
		  					</tbody>
		  				</table>
		  			</div>
		  		</div>
		  	</div>	
		  </div>
		  <div class="tab-pane" id="payment_info_tab_pane">
		  	<div class="row">
		  		<div class="col-xs-12 col-sm-6 col-md-12">
		  			<div id="table_container" class="well clearfix">
		  				<table class="table table-bordered">
		  					<thead>
		  						<tr>
			  						<th> Primary </th>
			  						<th> Type </th>
			  						<th> Credit Card Number </th>
			  						<th> Expiration Date </th>
			  						<th> Billing Zip/Post Code </th>
			  						<th> Actions </th>
		  						</tr>
		  					</thead>
		  					<tbody>
		  					@if(!empty($transactions))
		  						@foreach($transactions as $key => $value)
		  							<tr>
		  								<td align="center"> (Primay Radio Button) </td>
		  								<td align="center"> Visa/MasterCard </td>
		  								<td align="center"> XXXX-XXXX-XXXX-1234 </td>
		  								<td align="center"> 99/99 </td>
		  								<td align="center"> 19104</td>
		  								<td align="center"> - </td>
		  							</tr>
		  						@endforeach
		  					@endif
		  					</tbody>
		  				</table>
		  			</div>
		  		</div>
		  	</div>
		  </div>

		  </div>
		</div>



	</body>

	<script type="text/javascript">
	$(document).ready(function() {

	$('.nav-tabs a').click(function (e) {
	  e.preventDefault()
	  $(this).tab('show')
	})

	});

	</script>

</html>
