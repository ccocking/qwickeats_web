<div  class="well clearfix">
{{Form::open(['url' => "restaurants/{$restaurant_id}/checkout"])}}
<ol id="bag_item_list">
	@foreach ($cart as $item_number => $item)
		@foreach($item as $item_name => $item_properties ) 
		<li id="bag_item_name" class="pull-left">{{$item_name}}<span id="bag_item_price">${{$item_properties['price']}}<a href="#" data-index="{{{$item_index}}}" data-restaurant="{{{$restaurant_id}}}" id="delete_item_icon"><span id="delete_item_icon2" class="glyphicon glyphicon-remove-circle"></span></a></span>
		<span id="index_item" >{{$item_index++}}</span>
		</li>
			<br>
			@foreach($item_properties as $property_name => $property_value)
			@if($property_name == 'required_choices')
				@if(!empty($property_value))
				@foreach($property_value as $required_choice => $choice_value)
				<small id="bag_item_attr">{{ $choice_value}} </small>
				@endforeach
				@endif
			@elseif ($property_name == 'optional_choices') 
				@if(!empty($property_value))
				@foreach($property_value as $optional_choice => $optional_choice_price)
				<small id="bag_item_attr">{{ $optional_choice}} ${{$optional_choice_price}}</small>
				@endforeach
				@endif
			@elseif ($property_name == 'special_instruction')
				@if(strlen($property_value) > 0)
				<small id="bag_item_attr">Instructions: {{$property_value}}</small>
				@endif
			@endif
			@endforeach
		@endforeach
	@endforeach
	</ol>
	<hr>
	<p id="sub_total_line">Food Beverage Total<span>
	@if(!isset($sub_total))
	$0
	@else <span id="sub_total">${{$sub_total}}</span>
	@endif
	</span></p>
	<p id="sales_tax_line">Sales Tax<span> 
	@if(!isset($tax))
	$0
	@else <span id="tax"> ${{$tax}} </span>
	@endif
	</span></p>
	</span></p>
	<p id="tip_line">Tip: 
	<select name="tip">
	@for($i = 0;$i <= $tip_factor;$i = $i + 50)
		<option value={{$i}}>${{$i}}</option>
	@endfor
	</select>
	<span>
	</span>
	
	<hr>
	
	<span id="grand_total"> 
	<h3>Total
	@if(!isset($total))
	$0
	@else <span id="dollar_grand_total">${{$total}}</span>
	@endif
	</h3>
	</span>
	<hr>
	@if ($total > 0 && !$checkout_flag )
	{{ Form::hidden('total', $total)}}
	{{ Form::submit('Proceed To Checkout', array("id" => "checkout_button", "class" => "btn btn-danger"))}}
	@endif
</div>
{{Form::close()}}

<script>
$(document).off('click', '#delete_item_icon').on("click", "#delete_item_icon", function () {
		var delete_index = $(this).data('index');
		var restaurant_id = $(this).data('restaurant');
		
		var data_one =
		{
			"item_index" : delete_index,
			"restaurant_id" : restaurant_id
		};

  	var posting = $.ajax({
  		type: 'POST',
  		url: "/deleteitem", 
  		data: data_one
  	});

  	posting.done(function( data ) {
  	    var posting2 = $.post("/getcart",data_one);
  	    posting2.done(function(result) {
  	    $('#bag_details_container').empty().append(result);
  	    }
  	  );
  	});
});	

$( document ).ready(function() {
  var grand_total = $('#dollar_grand_total').text();
  var grand_total_int = parseFloat(Number(grand_total.replace(/[^0-9\.]+/g,"")));
  $('#tip_line').change(function() {
  	var tip = $('#tip_line :selected').text();
  	var tip_int =  parseFloat(Number(tip.replace(/[^0-9\.]+/g,"")));
  	$('#dollar_grand_total').html(grand_total_int+tip_int);
  });
});
</script>













