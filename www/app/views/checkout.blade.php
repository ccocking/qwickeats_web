<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title> QwickEats - Order Food Online! </title>
		<meta name="description" content="">
		<meta name="author" content="">

		<!-- Use the correct meta names below for your web application
			 Ref: http://davidbcalhoun.com/2010/viewport-metatag 
			 
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">-->
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="/smartadmin_css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="/smartadmin_css/smartadmin-production.css">
		<link rel="stylesheet" type="text/css" media="screen" href="/smartadmin_css/smartadmin-skins.css">

		<!-- SmartAdmin RTL Support is under construction
		<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.css"> -->

		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="css/your_style.css"> -->

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="/css/demo.css">
		<link rel="stylesheet" type="text/css" media="screen" href="/css/menu.css">
		<link rel="stylesheet" type="text/css" media="screen" href="/css/checkout.css">


		<!-- FAVICONS -->
		<link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="img/favicon/favicon.ico" type="image/x-icon">
		<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
	
		<!-- GOOGLE FONT -->
		<link href='http://fonts.googleapis.com/css?family=Playfair+Display+SC:400,700,900' rel='stylesheet' type='text/css'>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>

		<script type="text/javascript">
		    // This identifies your website in the createToken call below
	 			Stripe.setPublishableKey('pk_test_Jdg1un5foYnW4QXsxwnsV1ez');
		 
		    var stripeResponseHandler = function(status, response) {
		      var $form = $('#payment-form');
		 
		      if (response.error) {
		        // Show the errors on the form
		        $form.find('.payment-errors').text(response.error.message);
		        $form.find('button').prop('disabled', false);
		      } else {
		        // token contains id, last4, and card type
		        var token = response.id;
		        // Insert the token into the form so it gets submitted to the server
		        $form.append($('<input type="hidden" name="stripeToken" />').val(token));
		        // and re-submit
		        $form.get(0).submit();
		      }
		    };
		 
		    jQuery(function($) {
		      $('#payment-form').submit(function(e) {
		        var $form = $(this);
		 
		        // Disable the submit button to prevent repeated clicks
		        $form.find('button').prop('disabled', true);
		 
		        Stripe.card.createToken($form, stripeResponseHandler);
		 
		        // Prevent the form from submitting with the default action
		        return false;
		      });
		    });
		  </script>





	</head>
	<body data-checkout="{{$checkout}}"class="">

		<!-- HEADER -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as it --> 
							<h4 id="filter_results"> Order Information </h4>
				</span>
			</div>
			<nav id="bag_details_container">

			</nav>
		</aside>
		<header id="header">
			<!--<span id="logo"></span>-->

			<div id="logo-group">
				<span id="logo">
				<a href="/">
				<img src="/img/Logos/top_left.png"> 
				</a>
				</span>
				<!-- END AJAX-DROPDOWN -->
			</div>

			@if(Auth::check())

			<div class="btn-group" id="account_settings">
			  <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" >
			    My Account <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu" role="menu">
			    <li><a href="/users/{{{Auth::user()->_id}}}/account">Account Settings</a></li>
			    <li><a href="/order_history">Order History</a></li>
			    <li><a href="/saved_addresses">Saved Addresses</a></li>
			    <li><a href="/payment_info">Paymetn Info</a></li>
			  </ul>
		  </div>


			<span id="login-header-space"><a href="/logout" class="btn btn-danger">LOGOUT</a></span>
			@endif
		</header>
		<!-- MAIN PANEL -->

		<div id="main" role="main">

		
			<!-- RIBBON -->
			<div id="ribbon">
				<ol class="breadcrumb">
					<li class="aligncenter" id="menu_title">
					Checkout Details
					</li>
				</ol>
			</div>


			@if(!Auth::check())
			<form action="/checkout_new_user" method="POST" class="form-horizontal" >
			
			<div class="row">
			<div class="col-md-12">

			<legend>Create Your Account</legend>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="email">Email</label>  
			  <div class="col-md-5">
			  <input id="form_email" name="email" type="text" placeholder="Enter Your Email" class="form-control input-sm">
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="password">Password</label>  
			  <div class="col-md-5">
			  <input id="password" name="password" type="password" placeholder="Choose a Password" class="form-control input-sm" required="">  
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="retype_password">Re-Type Password</label>  
			  <div class="col-md-5">
			  <input id="retype_password" name="retype_password" type="password" placeholder="Retype Password" class="form-control input-sm" required="">  
			  </div>
			</div>
			@else 
			<form action="/checkout_existing_user" method="POST" class="form-horizontal" >
			@endif

			{{ Form::hidden('remember_token', '') }}

			<div class="row">
			<div class="col-md-6">
			<fieldset>

			<!-- Form Name -->
			<legend>Order Information</legend>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="first_name">First Name</label>  
			  <div class="col-md-5">
			  <input id="first_name" name="delivery_address[first_name]" type="text" placeholder="First Name" class="form-control input-sm">
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="last_name">Last Name</label>  
			  <div class="col-md-5">
			  <input id="last_name" name="delivery_address[last_name]" type="text" placeholder="Last Name" class="form-control input-sm" required="">  
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="phone_number">Phone Number</label>  
			  <div class="col-md-5">
			  <input id="phone_number" name="delivery_address[phone_number]" type="text" placeholder="Phone Number" class="form-control input-sm" required="">  
			  </div>
			</div>


			<div class="form-group" id="delivery_pickup">
			  <label class="col-md-4 control-label" for="email">Delivery Option</label>  
				    <span id="radio_buttons">
				    <label class="radio-inline" for="delivery_radio">
				      <input type="radio" name="delivery_pickup" id="delivery_radio" value="delivery" checked="checked">
				      Delivery
				    </label> 
				    <label class="radio-inline" for="pickup_radio">
				      <input type="radio" name="delivery_pickup" id="pickup_radio" value="pickup">
				      Pick Up
				    </label>
				    </span> 
			</div>

			<span id="delivery_option">
				<!-- Text input-->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="address_line1">Address 1</label>  
				  <div class="col-md-8">
				  <input id="address_line1" name="delivery_address[address_line1]" type="text" placeholder="Eg. 11 Ottawa Avenue" class="form-control input-sm" required="">  
				  </div>
				</div>

				<!-- Text input-->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="address_line2">Address 2</label>  
				  <div class="col-md-6">
				  <input id="address_line2" name="delivery_address[address_line2]" type="text" placeholder="Eg. Townhouse #2, APT: 3" class="form-control input-sm" required="">  
				  </div>
				</div>

				<!-- Select Basic -->
				<div class="form-group">
				  <label class="col-md-4 control-label" for="parish">Parish</label>
				  <div class="col-md-5">
				    <select id="parish" name="delivery_address[parish]" class="form-control">
				      <option value="Clarendon">Clarendon</option>
				      <option value="Hanover">Hanover</option>
				      <option value="Manchester">Manchester</option>
				      <option value="Portland">Portland</option>
				      <option value="St. Andrew/Kingston">St. Andrew/Kingston</option>
				      <option value="St. Ann">St. Ann</option>
				      <option value="St. Catherine">St. Catherine</option>
				      <option value="St. Elizabeth">St. Elizabeth</option>
				      <option value="St. James">St. James</option>
				      <option value="St. Mary">St. Mary</option>
				      <option value="St. Thomas">St. Thomas</option>
				      <option value="Westmorland">Westmorland</option>
				    </select>
				  </div>
				</div>
			</span>

			</div>


			<div class="col-md-6">
			<!-- Form Name -->
			<legend>Payment Information</legend>

			<!-- Multiple Radios (inline) -->
			<div class="form-group">
				    <label class="radio-inline" for="payment_option-0">
				      <input type="radio" name="payment_option" id="payment_option-0" value="credit_debit" checked="checked">
				      Credit/Debit Card
				    </label> 
				    <label class="radio-inline" for="payment_option-1">
				      <input type="radio" name="payment_option" id="payment_option-1" value="paypal">
				      Paypal
				    </label> 
				    <label class="radio-inline" for="payment_option-2">
				      <input type="radio" name="payment_option" id="payment_option-2" value="qe_voucher">
				      QwickEats Voucher
				    </label> 
				    <label class="radio-inline" for="payment_option-3">
				      <input type="radio" name="payment_option" id="payment_option-3" value="cash">
				      Cash
				    </label>
				</div>
		
			<span id="credit_debit_form">
				<span class="payment-errors"></span>
		 		<div class="form-group">
						<label>
							<span>Card Number</span>
							<input type="text" size="20" data-stripe="number"/>
						</label>

						<label>
							<span>CVC</span>
							<input type="text" size="4" data-stripe="cvc"/>
						</label>

						<label>
							<span>Expiration (MM/YYYY)</span>
							<input type="text" size="2" data-stripe="exp-month"/>
						</label>
						{{Form::hidden('restaurant_id',$restaurant_id)}}
						<span> / </span>
						<input type="text" size="4" data-stripe="exp-year"/>
						<br>
						<button id="submit_payment_button" class="btn btn-danger" type="submit">Submit Payment</button>
					</div>
				</span>
				</div>
			</div>
		</fieldset>
		<input type="hidden" name="subtotal" value=""  id="hidden_subtotal" input>
		<input type="hidden" name="tax" value="" id="hidden_tax" input>
		<input type="hidden" name="total" value=""  id="hidden_total" input>
	</form>
</body>



<script type="text/javascript">
	$(document).ready(function()
	 {

	 	var checkout = $('body').data('checkout');

  	var posting = $.post("/getcart",
  		{
  			checkout_flag : checkout
  		});
  	posting.done(function(result) 
  	{
    	$('#bag_details_container').empty().append(result);
    	//Get the subtotal values
    	var subtotal = $('#sub_total').text();
    	var tax = $('#tax').text();
    	var total = $('#dollar_grand_total').text();

    	$( "#hidden_subtotal" ).attr( "value", subtotal);
    	$( "#hidden_total" ).attr( "value",  total);
    	$( "#hidden_tax" ).attr( "value",  tax);
    });


    $('input[name="delivery_pickup"]').click(function()
    {
      if($(this).attr("value")=="delivery")
      {
       
       $("#delivery_option").show();  
      }
      if($(this).attr("value")=="pickup")
      {
       $("#delivery_option").hide(); 
      }
    });

    $('input[name="payment_option"]').click(function()
    {
      if($(this).attr("value")=="credit_debit")
      {
       $("#credit_debit_form").show();  
      }
      if($(this).attr("value")=="paypal")
      {
       $("#credit_debit_form").hide();  
      }
      if($(this).attr("value")=="qe_voucher")
      {
       $("#credit_debit_form").hide();  
      }
      if($(this).attr("value")=="cash")
      {
       $("#credit_debit_form").hide();  
      }
    });



	});	
</script>
<script src="/js/bootstrap/bootstrap.min.js"></script>	

</html>
