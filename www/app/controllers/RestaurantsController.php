<?php

class RestaurantsController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @todo Error codes for mishandled responses
	 * @return View - which is a search listing of all the restaurants the in the database
	 * @author Christian Cocking <cc@pith-studio.com>
 	 */
 	public function index()
	{
		if (Input::has('page'))
		{
			$page = Input::get('page');
		}

		// This is a saved Address query
		if (Input::has('saved_address'))
		{	
			$saved_address = Input::get('saved_address');
			$coordinates = User::getLonLat($saved_address);
			if ($coordinates)
			{
				$latitude = $coordinates['latitude'];
				$longitude = $coordinates['longitude'];

				Session::put('search_latitude', $latitude);
				Session::put('search_longitude', $longitude);
			} else
			{ // What to do if the saved address did not resolve (MAKE SURE IT RESOLVES)

			}


		}


		// This is a search query
		if (Input::has('location'));
		{

			$location = json_decode(Input::get('location'),true);
			$latitude = $location['k'];
			$longitude =  $location['B'];
			
			if (!empty($latitude) && !empty($longitude))
			{
				Session::put('search_latitude', $latitude);
				Session::put('search_longitude', $longitude);
			}
		}


		if(Session::has('cuisine_filters')) 
		{ // Session has cuinse filters
			$filters = json_decode(Session::get('cuisine_filters'));
			$restaurants = Restaurant::applyFiltersToAllResults($filters,$page);
			if ($restaurants)
			{ //Request sucessfully returned
				return View::make('search' , compact('restaurants'));
			} else 
			{ //TODO Return a resource not found error

			}
		} else 

		{
			$restaurants = Restaurant::applyFiltersToAllResults(null, $page);
			if ($restaurants)
			{ //Request sucessfully returned
				return View::make('search' , compact('restaurants'));
			} else 
			{ //TODO Return a resource not found error

			}
		}




	

		//Get all the restaurant objects in the database
		// $restaurants = Restaurant::getAllRestaurants();
		// Paginate results to be 10 per page
		// $restaurants = Restaurant::paginate(25);
	
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$restaurant = Restaurant::getResaturantByID($id);
		
		//Clears the cart if you view a different restaurant
		if (!Session::has('viewing_restaurant'))
		{
			Session::put('viewing_restaurant', $restaurant->name);

		} else 
		{
			if (Session::get('viewing_restaurant') != $restaurant->name)
			{
				if(Session::has('cart_id'))
				{
					$redis = Redis::connection();
					$redis_cart_key = Session::get('cart_id');
					$redis->del($redis_cart_key);
					Session::forget('cart_id');
				}
				Session::put('viewing_restaurant', $restaurant->name);
			}
		}
		$menu = Menu::getMenuByID($restaurant->menu_id);
		$menu_categories = $menu['categories'];
		if ($restaurant)
		{
			return View::make('menu', compact('restaurant','menu_categories'));
		}
	}

	public function apply_filter()
	{

		if(Input::has('filter'))
		{
			$new_filter = Input::get('filter');
			if (Session::has('cuisine_filters'))
			{
				$cuisine_filters = json_decode(Session::pull('cuisine_filters'),true);
				foreach (array_keys($cuisine_filters, $new_filter) as $key) {
				    unset($cuisine_filters[$key]);
				}
				$cuisine_filters[] = $new_filter;
				$cuisine_filters = json_encode($cuisine_filters);
				Session::put('cuisine_filters', $cuisine_filters);
			} else
			{
				$cuisine_filters[0] = $new_filter;
				$cuisine_filters = (json_encode($cuisine_filters));
				Session::put('cuisine_filters', $cuisine_filters);
			}
		}
		$live_filters = json_decode(Session::get('cuisine_filters'));
		$restaurants = Restaurant::applyFiltersToAllResults($live_filters,1);

		//return Response::Json(json_decode(Session::get('cuisine_filters')));

		return View::make('search_results',compact('restaurants'));
	}

	public function remove_filter()
	{

		if(Input::has('filter'))
		{
			$new_filter = Input::get('filter');
			if (Session::has('cuisine_filters'))
			{ // Remove that key from the array
				$cuisine_filters = json_decode(Session::pull('cuisine_filters'),true);
				
				foreach (array_keys($cuisine_filters, $new_filter) as $key) {
				    unset($cuisine_filters[$key]);
				}
				$cuisine_filters = json_encode($cuisine_filters);
				Session::put('cuisine_filters', $cuisine_filters);
			}
		}
		$live_filters = json_decode(Session::get('cuisine_filters'));

		if (count($live_filters) > 0)
		{
			$restaurants = Restaurant::applyFiltersToAllResults($live_filters,1);
		} else
		{
			if (Session::has('search_longitude') && Session::has('search_longitude'))
			{
			  $restaurants = Restaurant::applyFiltersToAllResults(null,1);
			} else
			{
				$restaurants = Restaurant::getAllRestaurants();
			}
		}
		return View::make('search_results',compact('restaurants'));
		}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	/**
	 * Returns a JSON response for the options of a given menu item
	 *
	 * @author Christian Cocking
	 * @param string $restuarnt_id
	 * @param string $menu_category
	 * @param string $menu_item
	 * @return HTML View For Modal
	 */

	public function menuItemOptions()
	{
		$restaurant_id = Input::get('restaurant_id');
		$menu_category = Input::get('menu_category');
		$menu_item_input = Input::get('menu_item');
		// Grab the restaurant object
		$restaurant = Restaurant::getResaturantByID($restaurant_id);
		// Get the Categories for that particular menu item
		$menu = Menu::getMenuByID($restaurant->menu_id);
		$categories = $menu['categories'];

		foreach($categories as $index => $category)
		{
			if ($category['name'] == $menu_category)
			{
				
				foreach ($category['menu_items'] as $index => $menu_item) 
				{
					if ($menu_item['name'] == $menu_item_input)
					{
					$menu_item_options = $menu_item['menu_item_options'];
					$item_price = $menu_item['price'];
					}
				}
			}			
		}
		// Return the JSON Response
		// return a view make of the modal
		return View::make('menu_item_options', compact('menu_item_options', 'menu_item_input', 'item_price','restaurant_id'));
	}

	public function CheckoutPage($id)
	{
		$restaurant_id = $id;
		$checkout = true;
		return View::make('checkout', compact('restaurant_id', 'checkout'));
	}
	/**
	 * Returns a JSON response for the options of a given menu item
	 *
	 * @todo check for potential price injection
	 * @todo account for the potential for the session value to expire/ enforce session variable expiry
	 * @author Christian Cocking
	 * @return JSON Response
	 */
	public function addtobag () 
	{	
		// Need to translate indexes to names 
		$redis = Redis::connection();

		if (!Session::has('cart_id'))
		{
			$random = Random::string(10);
			if($redis->exists($random))
			{
				while($redis->exists($random))
				{// Keep changing the random string until it does not exists
				$random = Random::string(10);
				}
			}
			Session::put('cart_id', $random);
			$cart = Restaurant::updateCart(null, Input::get('item_added'), Input::get('item_base_price'), Input::get('required'), Input::get('optional_items'), Input::get('special_instruction'));
			// $cart_entry_json = json_encode($cart_entry);
			$redis->set($random, $cart);
	} else
	{
		$redis_cart_key = Session::get('cart_id');
		$old_cart = $redis->get($redis_cart_key);
		$old_cart_decode = json_decode($old_cart);
		$cart = Restaurant::updateCart($old_cart_decode, Input::get('item_added'), Input::get('item_base_price'), Input::get('required'), Input::get('optional_items'), Input::get('special_instruction'));
		$redis->set($redis_cart_key, $cart);
	}
		$checkout_flag = 0;
		$cart = json_decode($cart, true);
		$sub_total = Restaurant::cartSubTotal($cart);
		$tip_factor = ceil(( $sub_total / 100 ) * 100) / 2;  
		$tax = Restaurant::cartTax($sub_total);
		$total = $sub_total + $tax;
		$tax = number_format(round($tax, 2),2);
		$sub_total = number_format($sub_total,2);
		$total = number_format(round($total, 2),2);
		$item_index = 0;
		$restaurant_id =  Input::get('restaurant_id');
		//return Redirect::to("restaurants/{$restaurant_id}/menu");
		return View::make('cart',compact('cart','sub_total','tax','total','item_index','tip_factor','restaurant_id','checkout_flag'));
	}

	public function getCart() 
	{
		$checkout_flag = 0;
		$restaurant_id = Input::get('restaurant_id');
		if (Session::has('cart_id'))
		{
			$redis = Redis::connection();
			$redis_cart_key = Session::get('cart_id');
			$cart = $redis->get($redis_cart_key);
			$cart = json_decode($cart, true);
			$sub_total = Restaurant::cartSubTotal($cart);
			$tip_factor = ceil(( $sub_total / 100 ) * 100) / 2;  
			$tax = Restaurant::cartTax($sub_total);
			$total = $sub_total + $tax;
			$tax = number_format(round($tax, 2),2);
			$sub_total = number_format($sub_total,2);
			$total = number_format(round($total, 2),2);
			if (Input::has('checkout_flag'))
			{
				$checkout_flag = Input::get('checkout_flag');
			}
			$item_index = 0;
			return View::make('cart',compact('cart','sub_total','tax','total','item_index','tip_factor','restaurant_id','checkout_flag'));
		}
	}

	public function deleteItem()
	{
		$item_index = Input::get('item_index');
		if (Session::has('cart_id'))
		{
			$redis = Redis::connection();
			$redis_cart_key = Session::get('cart_id');
			$cart = $redis->get($redis_cart_key);
			$cart = json_decode($cart, true);
			$new_cart = Restaurant::deleteFromCart($cart, $item_index);
			$redis->set($redis_cart_key, $new_cart);
			return Response::json($new_cart);
		}

	}

	public function redisJson()
	{
		$redis = Redis::connection();
		$redis_json_encoded = $redis->get('SOsooZiOfZ');
		$redis_json = json_decode($redis_json_encoded);
		return Response::json($redis_json);
		 
	}

}
