<?php

class UsersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		


	}


	/**
	 *
	 */

	public function createFromCheckout()
	{
 		$user = User::createUser(Input::only('email', 'password','remember_token','delivery_address'));
		$transaction = Transaction::processTransaction($user->_id, Input::get('restaurant_id'),Input::get('delivery_pickup'),Input::get('payment_option'),Input::get('total'),Input::get('delivery_address'),Input::get('subtotal'),Input::get('tax'));
		return Response::json($user);
	}

	public function checkoutExisting()
	{

		$user_id = Auth::id();
		$new_user = User::addDeliveryAddress($user_id, Input::get('delivery_address'));
		$transactions = Transaction::processTransaction($user_id,Input::get('restaurant_id'),Input::get('delivery_pickup'),Input::get('payment_option'),Input::get('total'),Input::get('delivery_address'),Input::get('subtotal'),Input::get('tax'));
		return Response::json($new_user);
	}

	public function login()
	{
		$remember_flag = false;
		if(Input::get('remember') ==  "on")
		{
			$remember_flag = true;
		}

		if (Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password')), $remember_flag))
		{
			$user_id = Auth::id();
			return Redirect::to("{$user_id}/dashboard");
		}
		dd("Failed Login");
	}

	public function login_modal()
	{
		$redirect = Input::get('location');
		$remember_flag = false;
		if(Input::get('remember') ==  "on")
		{
			$remember_flag = true;
		}

		if (Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password')), $remember_flag))
		{
			$user_id = Auth::id();
			return Redirect::to($redirect);
		}
		dd("Failed Login");

	}

	public function dashboard ($id)
	{
		if(Auth::check())
		{
			$addresses = Auth::user()->delivery_addresses;
			foreach($addresses as $index => $values)
			{
				$address = $values['address_line1'];
				$saved_addresses[$address] = $values['address_line1'];
			}
		}

		$transactions = Transaction::getUserTransactions($id);
 		return View::make('dashboard', compact('transactions','saved_addresses'));
	}

	public function get_settings($id)
	{
		$transactions = Transaction::getUserTransactions($id);
		return View::make('settings', compact('transactions'));
	}

	public function update_account()
	{
		$user_id = Auth::id();
		$updated_user = User::updateSettings($user_id, Input::all());
		if ($updated_user)
		{

			return Redirect::to("users/$user_id/account");
		} else
		{ // Flash a failure message

		}
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
