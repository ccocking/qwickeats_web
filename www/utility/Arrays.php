<?php

class Arrays
{
	
	/**
	 * Collects the values in the subarray
	 * 
	 * @param string $path dot notation to reach value
	 * @param array $array array from which to collect
	 * @return collection of values
	 */
	public static function collect(array $array, $path) {
		$coll = array();
		foreach ($array as $elem) {
			$coll[] = self::get($elem,$path);
		}
		return $coll;
	}

	/**
	 * Tally the count of matching values of a given key (path)
	 * 
	 * @param string $path path to the value
	 * @param array $array array from which to tally
	 * @return array $tally tally of the values
	 */
	public static function tally(array $array, $path) {
		$tallies = array();
		foreach ($array as $elem) {
			$val = self::get($elem, $path);
			if (is_string($val) || is_int($val)) {
				@$tallies[$val]++;
			} else {
					// array key must be string or int
				@$tallies[null]++;
			}
		}
		return $tallies;
	}

	/**
	 * Get value from array via dot notation
	 * 
	 * @param array $a
	 * @param string $path
	 * @param 
	 */
	public static function get(array $a, $path, $default = null) {
		$current = $a;
		$p = strtok($path, '.');

		while ($p !== false) {
			if (!isset($current[$p])) {
				return $default;
			}
			$current = $current[$p];
			$p = strtok('.');
		}

		return $current;
	}

	/**
	 * Choose a random value from an array
	 * @param array $a array from which to choose
	 * @return mix $value random value from array
	 */
	public static function random(array $a) {
		$keys = array_keys($a);
		$key = $keys[rand(0,count($keys)-1)];
		return $a[$key];
	}

	/**
	 * Converts an array to a stdClass object
	 * @param array $a the array to convert
	 * @return stdClass object
	 */
	public static function objectify($a) {
		if (is_array($a)) {
			$o = new stdClass();
			foreach ($a as $k => $v)
				$o->$k = $v;
			return $o;
		}
		else {
			// Return object
			return $a;
		}
	}
	
	/**
		 * Compares two arrays to see if they contain the same array_values
		 * 
		 * @param array $a1 first array to compare 
		 * @param array $a2 second array to compare
		 * @return boolean true if both arrays contain the same values, regardless of keys
		 */	
	public static function sameValues($a1, $a2) {
		return !array_diff($a1, $a2) && !array_diff($a2, $a1);
	}
}
