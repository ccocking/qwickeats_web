<?php

/*
 * Utility functions related to randomness
 */
class Random
{

	const BLOWFISH_CHARSET = './acbdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
	const ALPHA_NUMERIC = 'acbdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
	const NUMERIC = '1234567890';
	const UNAMBIGUOUS_UPPER_LOWER = '23456789abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ';
	const UNAMBIGUOUS_UPPER = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ';
	const UNAMBIGUOUS_LOWER = '23456789abcdefghijkmnpqrstuvwxyz';

	/**
	 * Generates a random string from the charset given, alpha-numeric by default
	 */
	public static function string($length = 10, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
	{
		$str = '';
		for ($i = 0; $i < $length; $i++)
			$str .= substr($chars, mt_rand(0,strlen($chars)-1), 1);
		return $str;
	}

	/**
	 * Get random value out of an array
	 */
	public static function element(array $array)
	{
		return $array[array_rand($array)];
	}
}
