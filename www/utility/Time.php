<?php

class Time {

	const SUNDAY    = 0;
	const MONDAY    = 1;
	const TUESDAY   = 2;
	const WEDNESDAY = 3;
	const THURSDAY  = 4;
	const FRIDAY    = 5;
	const SATURDAY  = 6;
	
	/**
	 * Converts a numeric representation of the day of the week, i.e., date('w') to 
	 * a full textual representation of the day of the week, i.e., date('l')
	 * 
	 * @param numeric representation of the day of the week
	 * @return full textual representation of the day of th week
	 */
	public static function dow($w, $style = 'long_name') {

		// days of the week
		static $dows = [
			0 => ['short_name' => 'Sun',  'long_name' => 'Sunday'],
			1 => ['short_name' => 'Mon',  'long_name' => 'Monday'],
			2 => ['short_name' => 'Tue',  'long_name' => 'Tuesday'],
			3 => ['short_name' => 'Wed',  'long_name' => 'Wednesday'],
			4 => ['short_name' => 'Thur', 'long_name' => 'Thursday'],
			5 => ['short_name' => 'Fri',  'long_name' => 'Friday'],
			6 => ['short_name' => 'Sat',  'long_name' => 'Saturday'],
		];

		// no sybase_fetch_array(result) checks
		return $dows[$w][$style];
	}

	/**
	 * Return a list of timezone for populating a dropdown
	 * 
	 * @return array
	 */
	public static function timezones()
	{
		$timezones = DateTimeZone::listIdentifiers(DateTimeZone::AMERICA);
 		$timezones = array_combine($timezones, $timezones);
 		return $timezones;
	}
}
